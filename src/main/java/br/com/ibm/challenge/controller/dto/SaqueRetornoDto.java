package br.com.ibm.challenge.controller.dto;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SaqueRetornoDto {

	private String mensagemRetorno;
	
	private List<CedulaRetornoDto> cedulasRetorno;
	
}

package br.com.ibm.challenge.controller.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CedulaRetornoDto {

	private Integer quantidade;
	
	private Integer cedula;
}

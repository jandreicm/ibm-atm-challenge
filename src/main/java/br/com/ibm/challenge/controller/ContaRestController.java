package br.com.ibm.challenge.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ibm.challenge.controller.dto.ExtratoContaDto;
import br.com.ibm.challenge.domain.Conta;
import br.com.ibm.challenge.service.ContaService;

@RestController
@RequestMapping("conta")
public class ContaRestController {

	@Autowired
	private ContaService contaService;

	@PostMapping
	public Conta nova(@RequestBody Conta conta) {
		var retorno = contaService.criarConta(conta);

		return retorno;
	}

	@GetMapping
	public Page<Conta> lista(@RequestParam(value = "page", required = false, defaultValue = "0") int page,
			@RequestParam(value = "size", required = false, defaultValue = "10") int size) {
		var caixas = contaService.listar(page, size);
		return caixas;
	}

	@GetMapping("extrato")
	public ExtratoContaDto extrato(
			@RequestParam(value = "quantidadeDias", required = true, defaultValue = "30") Long quantidadeDias,
			@RequestParam(value = "conta", required = true, defaultValue = "") String conta) {

		return contaService.extratoPorCaixa(quantidadeDias, UUID.fromString(conta));
	}
}

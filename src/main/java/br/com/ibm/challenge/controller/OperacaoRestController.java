package br.com.ibm.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ibm.challenge.controller.dto.ComprovanteRetornoDto;
import br.com.ibm.challenge.controller.dto.SaqueRetornoDto;
import br.com.ibm.challenge.domain.Movimentacao;
import br.com.ibm.challenge.domain.Transferencia;
import br.com.ibm.challenge.service.DepositoService;
import br.com.ibm.challenge.service.SaqueService;
import br.com.ibm.challenge.service.TransferenciaService;

@RestController
@RequestMapping("operacao")
public class OperacaoRestController {
	
	@Autowired
	private SaqueService saqueService; 
	
	@Autowired
	private DepositoService depositoService; 
	
	@Autowired
	private TransferenciaService transferenciaService; 
	
	@PostMapping("saque")
	public SaqueRetornoDto saque(@RequestBody Movimentacao movimentacao) {
		var retorno = saqueService.sacar(movimentacao);
		
		return retorno;
	}
	
	@PostMapping("deposito")
	public ComprovanteRetornoDto deposito(@RequestBody Movimentacao movimentacao) {
		var retorno = depositoService.deposito(movimentacao);

		return retorno;
	}

	@PostMapping("transfere")
	public ComprovanteRetornoDto transfere(@RequestBody Transferencia movimentacao) {
		var retorno = transferenciaService.transferencia(movimentacao);
		return retorno;
	}

}

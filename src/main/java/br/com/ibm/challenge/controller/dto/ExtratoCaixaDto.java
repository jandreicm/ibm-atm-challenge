package br.com.ibm.challenge.controller.dto;

import java.math.BigDecimal;
import java.util.List;

import br.com.ibm.challenge.domain.Caixa;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter @Setter
@Builder
public class ExtratoCaixaDto {
	private Caixa caixa ;
	private BigDecimal vlrSaldoInicial;
	private BigDecimal vlrSaldoFinal;
	private List<ExtratoMovimentacaoCaixaItemDto> movimentacoes;
}

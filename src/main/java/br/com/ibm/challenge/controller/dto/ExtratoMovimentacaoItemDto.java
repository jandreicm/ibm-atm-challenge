package br.com.ibm.challenge.controller.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.ibm.challenge.domain.type.TipoMovimentacao;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@Builder
public class ExtratoMovimentacaoItemDto {

	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate data;
	private BigDecimal vlrMovimentacao;
	private TipoMovimentacao tpMovimentacao;
	
}

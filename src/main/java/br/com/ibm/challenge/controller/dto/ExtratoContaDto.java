package br.com.ibm.challenge.controller.dto;

import java.util.List;

import br.com.ibm.challenge.domain.Conta;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter @Setter
@Builder
public class ExtratoContaDto {
	private Conta conta;
	private List<ExtratoMovimentacaoItemDto> movimentacoes;
}

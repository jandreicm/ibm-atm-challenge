package br.com.ibm.challenge.controller.dto;

import java.time.LocalDateTime;

import javax.persistence.EntityNotFoundException;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class ApiError {

	public ApiError(HttpStatus status, String error, Exception ex2) {
		this(status);
		this.message = error;
		this.debugMessage = ex2.getLocalizedMessage();
	}
	
	public ApiError(HttpStatus status) {
		this.status = status;
		this.statusCode = status.value();
	}

	public ApiError(HttpStatus notFound, EntityNotFoundException ex2) {
		this(notFound);
		this.message = ex2.getMessage();
		this.debugMessage = ex2.getLocalizedMessage();
	}

	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private LocalDateTime dateTime = LocalDateTime.now();
	private String message;
	
	@JsonInclude(Include.NON_NULL)
	private String debugMessage;
	
	private HttpStatus status;
	private Integer statusCode;
	
	@JsonInclude(Include.NON_NULL)
	private Object constraintValidation;

}

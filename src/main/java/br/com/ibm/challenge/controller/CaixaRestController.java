package br.com.ibm.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ibm.challenge.controller.dto.AbreFechaCaixaDto;
import br.com.ibm.challenge.controller.dto.ExtratoCaixaDto;
import br.com.ibm.challenge.domain.Caixa;
import br.com.ibm.challenge.service.CaixaService;

@RestController
@RequestMapping("caixa")
public class CaixaRestController {
	
	@Autowired
	private CaixaService caixaService; 
	
	
	@PostMapping("abrir")
	public Caixa abrir(@RequestBody AbreFechaCaixaDto abreCaixaDto) {
		Caixa caixa = Caixa.builder()
				.data(abreCaixaDto.getData())
				.vlrSaldoInicial(abreCaixaDto.getVlrSaldoInicial())
				.build();
		
		var caixaAberto = caixaService.abrirCaixa(caixa);
		return caixaAberto;
	}
	
	@PostMapping("criar")
	public Caixa criar(@RequestBody AbreFechaCaixaDto abreCaixaDto) {
		Caixa caixa = Caixa.builder()
				.data(abreCaixaDto.getData())
				.vlrSaldoInicial(abreCaixaDto.getVlrSaldoInicial())
				.build();
		
		var caixaAberto = caixaService.criarCaixaSeNaoExiste(caixa);
		return caixaAberto;
	}

	@PostMapping("fechar")
	public Caixa fechar(@RequestBody AbreFechaCaixaDto fechaCaixaDto) {
		Caixa caixa = Caixa.builder()
				.data(fechaCaixaDto.getData())
				.build();
		
		var caixaFechado = caixaService.fecharCaixa(caixa);
		return caixaFechado;
	}

	@GetMapping("lista")
	public Page<Caixa> lista(
			@RequestParam(
		            value = "page",
		            required = false,
		            defaultValue = "0") int page,
				@RequestParam(
		            value = "size",
		            required = false,
		            defaultValue = "10") int size) {
		
		var caixas = caixaService.listar(page, size);
		
		return caixas;
	}

	@GetMapping("atual")
	public Caixa atual() {
		var caixa = caixaService.findCaixaAberto();
		return caixa;
	}
	
	@GetMapping("extrato")
	public ExtratoCaixaDto extrato(
			@RequestParam(value = "quantidadeDias", required = true, defaultValue = "30") Long quantidadeDias,
			@RequestParam(value = "caixa", required = false) String caixa) {

		return caixaService.extrato(quantidadeDias, caixa);
	}

}

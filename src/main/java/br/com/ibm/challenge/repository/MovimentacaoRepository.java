package br.com.ibm.challenge.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ibm.challenge.domain.Caixa;
import br.com.ibm.challenge.domain.Conta;
import br.com.ibm.challenge.domain.Movimentacao;

@Repository
public interface MovimentacaoRepository extends JpaRepository<Movimentacao, UUID>{

	@Query(" select a "
			+ " from Movimentacao a "
			+ " left join fetch a.conta c "
			+ " left join fetch a.caixa ca "
			+ " left join fetch a.transferencia t "
			+ " left join fetch t.contaOrigem "
			+ " left join fetch t.contaDestino "
			+ " where a.conta = :conta "
			+ " order by a.dtInclusao desc ")
	public List<Movimentacao> extratoPorConta(@Param("conta") Conta conta);

	@Query(" select a "
			+ " from Movimentacao a "
			+ " left join fetch a.conta c "
			+ " left join fetch a.caixa ca "
			+ " left join fetch a.transferencia t "
			+ " left join fetch t.contaOrigem "
			+ " left join fetch t.contaDestino "
			+ " where a.caixa = :caixa "
			+ " order by a.dtInclusao desc ")
	public List<Movimentacao> extratoPorCaixa(@Param("caixa") Caixa caixa);

	@Query(" select sum(a.vlrMovimento) "
			+ " from Movimentacao a "
			+ " where a.caixa = :caixa ")
	public BigDecimal somaPorCaixa(@Param("caixa") Caixa caixa);

	
}

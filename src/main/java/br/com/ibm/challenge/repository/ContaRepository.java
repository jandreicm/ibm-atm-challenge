package br.com.ibm.challenge.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ibm.challenge.domain.Conta;

@Repository
public interface ContaRepository extends JpaRepository<Conta, UUID>{
	
	public boolean existsByTxtCliente(String txtCliente);
	
}

package br.com.ibm.challenge.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ibm.challenge.domain.Transferencia;

@Repository
public interface TransferenciaRepository extends JpaRepository<Transferencia, UUID>{

	

}

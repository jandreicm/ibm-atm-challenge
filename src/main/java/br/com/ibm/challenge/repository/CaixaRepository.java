package br.com.ibm.challenge.repository;

import java.time.LocalDate;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ibm.challenge.domain.Caixa;
import br.com.ibm.challenge.domain.type.StatusCaixa;

@Repository
public interface CaixaRepository extends JpaRepository<Caixa, UUID>{

	public boolean existsByData(LocalDate data);
	public Caixa findByData(LocalDate data);
	public boolean existsByStatusCaixa(StatusCaixa statusCaixa);
	public Caixa findOneByStatusCaixa(StatusCaixa statusCaixa);
	
	@Modifying(clearAutomatically = true)
	@Query("update Caixa caixa set caixa.statusCaixa=:statusNovo")
	void updateAllCaixasWithStatus(@Param("statusNovo") StatusCaixa statusNovo);
}

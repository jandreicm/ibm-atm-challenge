package br.com.ibm.challenge.service.exception;

public class SaldoContaInsuficienteException extends BaseException {
	public SaldoContaInsuficienteException() {
		super("Saldo insuficiente");
	}

}

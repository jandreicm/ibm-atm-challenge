package br.com.ibm.challenge.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.ibm.challenge.controller.dto.ExtratoContaDto;
import br.com.ibm.challenge.controller.dto.ExtratoMovimentacaoItemDto;
import br.com.ibm.challenge.domain.Conta;
import br.com.ibm.challenge.domain.Movimentacao;
import br.com.ibm.challenge.repository.ContaRepository;
import br.com.ibm.challenge.repository.MovimentacaoRepository;
import br.com.ibm.challenge.service.exception.ContaJaExisteException;
import br.com.ibm.challenge.service.exception.ContaObrigatorioException;
import br.com.ibm.challenge.service.exception.SaldoContaInsuficienteException;
import br.com.ibm.challenge.service.exception.SaldoContaZeradoException;

@Service
public class ContaService {

	private ContaRepository contaRepository;
	
	private MovimentacaoRepository movimentacaoRepository;
	
	@Autowired
	public void init(ContaRepository contaRepository, MovimentacaoRepository movimentacaoRepository) {
		this.contaRepository = contaRepository;
		this.movimentacaoRepository = movimentacaoRepository;
	}
	
	@Transactional
	public Conta criarConta(Conta conta) {
		conta.setId(null);
		conta.setVlrSaldo(BigDecimal.ZERO);
		
		validaCriaConta(conta);
		
		conta = contaRepository.save(conta);
		
		return conta;
	}
	
	private void validaCriaConta(Conta conta) {
		Boolean existeContaComEsseCliente = contaRepository.existsByTxtCliente(conta.getTxtCliente());
		if (existeContaComEsseCliente) {
			throw new ContaJaExisteException();
		}
	}

	/**
	 * Efetua validações necessárias para saque e salva o novo saldo da conta.
	 * 
	 * @param movimentacao
	 */
	public void ajustaSaldoContaNoSaque(Movimentacao movimentacao, Conta conta) {
		validaContaObrigatoria(movimentacao);

		validaAjusteSaldoContaParaSaque(movimentacao, conta);
		
		ajustaSaldoContaPorTipoMovimentacao(movimentacao, conta);
		
		contaRepository.save(conta);
	}

	public void ajustaSaldoContaNoDeposito(Movimentacao movimentacao, Conta conta) {
		validaContaObrigatoria(movimentacao);
		
		ajustaSaldoContaPorTipoMovimentacao(movimentacao, conta);
		
		contaRepository.save(conta);
	}

	public void ajustaSaldoContaPorTipoMovimentacao(Movimentacao movimentacao, Conta conta) {
		Boolean naoTemValorSaldo = (conta.getVlrSaldo() == null);
		if (naoTemValorSaldo) {
			conta.setVlrSaldo(new BigDecimal("0"));
		}
		
		switch (movimentacao.getTipoMovimentacao()) {
		case SAQUE:
			conta.setVlrSaldo(conta.getVlrSaldo().subtract(movimentacao.getVlrMovimento().abs()));
			break;
		case DEPOSITO_CHEQUE:
			conta.setVlrSaldo(conta.getVlrSaldo().add(movimentacao.getVlrMovimento().abs()));
			break;
		case DEPOSITO_DINHEIRO:
			conta.setVlrSaldo(conta.getVlrSaldo().add(movimentacao.getVlrMovimento().abs()));
			break;
		case TRANSFERENCIA:
			Boolean valorSaiDaContaOrigem = conta.equals(movimentacao.getTransferencia().getContaOrigem());
			Boolean valorEntraNaContaDestino= conta.equals(movimentacao.getTransferencia().getContaDestino());
			
			if (valorSaiDaContaOrigem) {
				conta.setVlrSaldo(conta.getVlrSaldo().subtract(movimentacao.getVlrMovimento().abs()));
			}else if (valorEntraNaContaDestino) {
				conta.setVlrSaldo(conta.getVlrSaldo().add(movimentacao.getVlrMovimento().abs()));
			}
			
			break;
		default:
			break;
		}
	}

	private void validaAjusteSaldoContaParaSaque(Movimentacao movimentacao, Conta contaNova) {
		validaContaObrigatoria(movimentacao);
		
		Boolean contaSaldoNulo = contaNova.getVlrSaldo() == null;
		Boolean contaSaldoZero = !contaSaldoNulo && contaNova.getVlrSaldo().equals(BigDecimal.ZERO);
		if (contaSaldoNulo || contaSaldoZero) {
			throw new SaldoContaZeradoException();
		}

		// valor da conta <= valor solicitado na movimentacao
		Boolean valorContaEhMenorIgualAZero = contaNova.getVlrSaldo().compareTo(BigDecimal.ZERO) <= 0;
		Boolean valorContaMaiorQueSolicitado = contaNova.getVlrSaldo().compareTo(movimentacao.getVlrMovimento()) < 0;
		if (valorContaMaiorQueSolicitado || valorContaEhMenorIgualAZero) {
			throw new SaldoContaInsuficienteException();
		}
	}
	
	private void validaContaObrigatoria(Movimentacao movimentacao) {
		Boolean naoTemContaInformada = movimentacao.getConta() == null;
		if (naoTemContaInformada) {
			throw new ContaObrigatorioException();
		}
		
	}

	public Conta findById(UUID id) {
		return contaRepository.getOne(id);
	}
	
	public Page<Conta> listar(int page, int size) {
		Pageable pageble = PageRequest.of(page, size);
		return contaRepository.findAll(pageble);
	}
	
	@Transactional
	public ExtratoContaDto extratoPorCaixa(Long quantidadeDias, UUID conta) {
		var aPartirDe = LocalDateTime.now().minusDays(quantidadeDias);

		aPartirDe = aPartirDe.withHour(0);
		aPartirDe = aPartirDe.withMinute(0);
		aPartirDe = aPartirDe.withSecond(0);
		
		//var movimentacoes = movimentacaoRepository.extrato(aPartirDe, conta);
		var contaFiltro = contaRepository.getOne(conta);
		var movimentacoes = movimentacaoRepository.extratoPorConta(contaFiltro);
		Hibernate.initialize(contaFiltro);
		Hibernate.initialize(movimentacoes);
		
		var extratoItem = movimentacoes.stream().map(mov -> {
			return ExtratoMovimentacaoItemDto.builder()
					.tpMovimentacao(mov.getTipoMovimentacao())
					.data(mov.getDtInclusao().toLocalDate())
					.vlrMovimentacao(mov.getVlrMovimento())
					.build();
		}).collect(Collectors.toList());
		
		var extrato = ExtratoContaDto.builder()
			.conta(contaFiltro)
			.movimentacoes(extratoItem)
			.build();
		
		return extrato;
	}
	
}

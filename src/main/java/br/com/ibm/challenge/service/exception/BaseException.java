package br.com.ibm.challenge.service.exception;

public class BaseException extends RuntimeException {
	
	private static final long serialVersionUID = -8230863929178870291L;

	public BaseException() {
	}

	public BaseException(String mensagem) {
		super(mensagem);
	}

	

}

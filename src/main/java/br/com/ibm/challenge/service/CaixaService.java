package br.com.ibm.challenge.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.ibm.challenge.controller.dto.ExtratoCaixaDto;
import br.com.ibm.challenge.controller.dto.ExtratoMovimentacaoCaixaItemDto;
import br.com.ibm.challenge.domain.Caixa;
import br.com.ibm.challenge.domain.Movimentacao;
import br.com.ibm.challenge.domain.type.StatusCaixa;
import br.com.ibm.challenge.repository.CaixaRepository;
import br.com.ibm.challenge.repository.MovimentacaoRepository;
import br.com.ibm.challenge.service.exception.CaixaAbertoDiferenteDoInformadoException;
import br.com.ibm.challenge.service.exception.CaixaNaoExisteException;
import br.com.ibm.challenge.service.exception.CaixaObrigatorioException;

@Service
public class CaixaService {

	private CaixaRepository caixaRepository;
	private MovimentacaoRepository movimentacaoRepository;
	
	@Autowired
	public void init(CaixaRepository caixaRepository, MovimentacaoRepository movimentacaoRepository) {
		this.caixaRepository = caixaRepository;
		this.movimentacaoRepository = movimentacaoRepository;
	}
	
	@Transactional
	public Caixa abrirCaixa(Caixa caixaEntrada) {
		Caixa caixa = caixaRepository.findByData(caixaEntrada.getData());
		
		validaSeCaixaExiste(caixa);
		
		Boolean saldoInicialZerado = (caixa.getVlrSaldoInicial() == null);
		if (saldoInicialZerado) {
			caixa.setVlrSaldoInicial(BigDecimal.ZERO);
		}
		
		caixa.setVlrSaldoInicial(caixaEntrada.getVlrSaldoInicial());
		caixa.setStatusCaixa(StatusCaixa.ABERTO);
		
		caixa = caixaRepository.save(caixa);
		
		return caixa;
	}
	
	/**
	 * se existir retorna o registro do banco e se nao existir cria.
	 * @param caixaEntrada
	 * @return
	 */
	@Transactional
	public Caixa criarCaixaSeNaoExiste(Caixa caixaEntrada) {
		Caixa caixa = caixaRepository.findByData(caixaEntrada.getData());
		
		Boolean existeCaixaParaData = caixa != null;
		if (existeCaixaParaData) {
			return caixa;
		}

		caixa = Caixa.builder()
					.data(caixaEntrada.getData())
					.statusCaixa(StatusCaixa.FECHADO)
					.vlrSaldoInicial(caixaEntrada.getVlrSaldoInicial())
					.build();
		
		caixa = caixaRepository.save(caixa);
		
		return caixa;
	}
	
	@Transactional
	public Caixa fecharCaixa(Caixa caixaEntrada) {
		validaFecharCaixa(caixaEntrada.getData());
		
		Caixa caixa = caixaRepository.findByData(caixaEntrada.getData());
		BigDecimal vlrSomaCaixa = movimentacaoRepository.somaPorCaixa(caixa);
		Boolean naoTemSomaCaixa = (vlrSomaCaixa == null);
		if (naoTemSomaCaixa) {
			vlrSomaCaixa = BigDecimal.ZERO;
		}
		
		//somando saldo inicial + movimentacoes do dia
		caixa.setVlrSaldoFinal(vlrSomaCaixa.add(caixa.getVlrSaldoInicial()));
		caixa.setStatusCaixa(StatusCaixa.FECHADO);
		
		caixaRepository.save(caixa);
		
		return caixa;
	}
	
	private void validaFecharCaixa(LocalDate data) {
		Boolean naoExisteCaixaAberto = !caixaRepository.existsByStatusCaixa(StatusCaixa.ABERTO);
		if (naoExisteCaixaAberto) {
			throw new CaixaNaoExisteException("Não existe caixa aberto para fechar");
		}
		
		Boolean naoExisteCaixaParaData = !caixaRepository.existsByData(data);
		if (naoExisteCaixaParaData) {
			throw new CaixaNaoExisteException("Caixa solicitado para fechamento não existe");
		}
	}
	
	private void validaSeCaixaExiste(Caixa caixa) {
		if (caixa == null) {
			throw new CaixaNaoExisteException("Esse caixa não existe, crie ele para poder abrir.");
		}
	}

	public void validaCaixaObrigatorio(Movimentacao saque, Caixa caixa) {
		Boolean naoInformouCaixa = saque.getCaixa() == null;
		if (naoInformouCaixa) {
			throw new CaixaObrigatorioException();
		}
		
		Boolean naoExisteCaixaParaData = (caixa == null);
		if (naoExisteCaixaParaData) {
			throw new CaixaNaoExisteException("Data de caixa informado não foi encontrado");
		}
		
		Caixa caixaAtual = caixaRepository.findOneByStatusCaixa(StatusCaixa.ABERTO);
		Boolean caixaAtualDiferenteCaixaInformado = !caixa.equals(caixaAtual);
		if (caixaAtualDiferenteCaixaInformado) {
			throw new CaixaAbertoDiferenteDoInformadoException();
		}
	}
	
	public Caixa findByData(LocalDate data) {
		return caixaRepository.findByData(data);
	}
	
	public Caixa findCaixaAberto() {
		var caixa = caixaRepository.findOneByStatusCaixa(StatusCaixa.ABERTO);
		return caixa;
	}

	public Page<Caixa> listar(Integer page, Integer size ) {
		Pageable pageble = PageRequest.of(page, size);
		return caixaRepository.findAll(pageble);
	}
	

	@Transactional
	public ExtratoCaixaDto extrato(Long quantidadeDias, String caixaIdString) {
		validaParametroExtrato(caixaIdString);
		var caixa = UUID.fromString(caixaIdString);
		
		var aPartirDe = LocalDateTime.now().minusDays(quantidadeDias);

		aPartirDe = aPartirDe.withHour(0);
		aPartirDe = aPartirDe.withMinute(0);
		aPartirDe = aPartirDe.withSecond(0);
		
		var caixaFiltro = caixaRepository.getOne(caixa);
		var movimentacoes = movimentacaoRepository.extratoPorCaixa(caixaFiltro);
		Hibernate.initialize(caixaFiltro);
		Hibernate.initialize(movimentacoes);
		
		var extratoItem = movimentacoes.stream().map(mov -> {
			return ExtratoMovimentacaoCaixaItemDto.builder()
					.tpMovimentacao(mov.getTipoMovimentacao())
					.data(mov.getDtInclusao().toLocalDate())
					.conta(mov.getConta())
					.vlrMovimentacao(mov.getVlrMovimento())
					.build();
		}).collect(Collectors.toList());
		
		var extrato = ExtratoCaixaDto.builder()
			.caixa(caixaFiltro)
			.movimentacoes(extratoItem)
			.build();
		
		return extrato;
	}

	private void validaParametroExtrato(String caixaIdString) {
		Boolean caixaIdNaoInformado = StringUtils.isEmpty(caixaIdString);
		if (caixaIdNaoInformado) {
			throw new CaixaObrigatorioException();
		}
		
	}

}

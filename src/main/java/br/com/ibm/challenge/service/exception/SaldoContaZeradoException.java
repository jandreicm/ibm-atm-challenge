package br.com.ibm.challenge.service.exception;

public class SaldoContaZeradoException extends BaseException {
	public SaldoContaZeradoException() {
		super("Saldo da conta esta zerado");
	}
}

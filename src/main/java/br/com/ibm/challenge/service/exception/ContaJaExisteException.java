package br.com.ibm.challenge.service.exception;

public class ContaJaExisteException extends BaseException {
	public ContaJaExisteException() {
		super("Conta já existente");
	}

}

package br.com.ibm.challenge.service.exception;

public class CaixaObrigatorioException extends BaseException {
	public CaixaObrigatorioException() {
		super("Caixa obrigatório");
	}
}

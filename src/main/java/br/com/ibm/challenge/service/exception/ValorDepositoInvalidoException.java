package br.com.ibm.challenge.service.exception;

public class ValorDepositoInvalidoException extends BaseException {
	public ValorDepositoInvalidoException() {
		super("Valor de depósito inválido");
	}
}

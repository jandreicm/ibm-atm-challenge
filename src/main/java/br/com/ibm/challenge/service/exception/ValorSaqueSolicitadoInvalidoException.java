package br.com.ibm.challenge.service.exception;

public class ValorSaqueSolicitadoInvalidoException extends BaseException {
	public ValorSaqueSolicitadoInvalidoException() {
		super("Valor solicitado é inválido");
	}

}

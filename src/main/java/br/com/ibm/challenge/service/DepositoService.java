package br.com.ibm.challenge.service;


import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ibm.challenge.controller.dto.ComprovanteRetornoDto;
import br.com.ibm.challenge.domain.Movimentacao;
import br.com.ibm.challenge.domain.Transferencia;
import br.com.ibm.challenge.domain.type.TipoMovimentacao;
import br.com.ibm.challenge.repository.MovimentacaoRepository;
import br.com.ibm.challenge.service.exception.TipoMovimentacaoObrigatorioException;
import br.com.ibm.challenge.service.exception.ValorDepositoInvalidoException;

@Service
public class DepositoService {

	private MovimentacaoRepository movimentacaoRepository;
	private ContaService contaService;
	private CaixaService caixaService;

	@Autowired
	public void init(MovimentacaoRepository movimentacaoRepository, ContaService contaService,
			CaixaService caixaService) {
		this.movimentacaoRepository = movimentacaoRepository;
		this.contaService = contaService;
		this.caixaService = caixaService;
	}

	@Transactional
	public ComprovanteRetornoDto deposito(Movimentacao movimentacao) {
		validaMovimentacaoDeposito(movimentacao);

		var caixa = caixaService.findByData(movimentacao.getCaixa().getData());
		var conta = contaService.findById(movimentacao.getConta().getId());
		movimentacao.setConta(conta);
		
		caixaService.validaCaixaObrigatorio(movimentacao, caixa);
		contaService.ajustaSaldoContaNoDeposito(movimentacao, conta);

		movimentacao.setCaixa(caixa);
		movimentacao.setConta(conta);
		movimentacao.setVlrMovimento(movimentacao.getVlrMovimento().abs());
		
		movimentacaoRepository.save(movimentacao);
		
		var depositoRetorno = montaMensagemDeposito(movimentacao);

		return depositoRetorno;
	}
	
	public void deposito(Transferencia transferencia) {
		var caixa = caixaService.findCaixaAberto();
		
		var movimentacao = Movimentacao.builder()
				.caixa(caixa)
				.conta(transferencia.getContaDestino())
				.vlrMovimento(transferencia.getVlrTransferencia())
				.tipoMovimentacao(TipoMovimentacao.TRANSFERENCIA)
				.transferencia(transferencia)
				.build();
		
		this.deposito(movimentacao);
	}

	private ComprovanteRetornoDto montaMensagemDeposito(Movimentacao movimentacao) {
		var nf = NumberFormat.getCurrencyInstance();
		nf.setGroupingUsed(true);
		nf.setMaximumFractionDigits(2);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy" );
		
		var mensagemRetorno = new StringBuilder();
		mensagemRetorno.append("Depósito efetuado com sucesso.");
		mensagemRetorno.append(String.format("\nConta Destino: %s", movimentacao.getConta()));
		mensagemRetorno.append(String.format("\nValor: %s",nf.format(movimentacao.getVlrMovimento().doubleValue())));
		mensagemRetorno.append(String.format("\nData: %s", formatter.format(movimentacao.getCaixa().getData())));
		
		var depositoRetorno = ComprovanteRetornoDto.builder()
				.mensagemRetorno(mensagemRetorno.toString())
				.build();
		return depositoRetorno;
	}

	private void validaMovimentacaoDeposito(Movimentacao saque) {
		Boolean naoTemValorMovinentacao = saque.getVlrMovimento() == null;
		if (naoTemValorMovinentacao) {
			throw new ValorDepositoInvalidoException();
		}

		Boolean naoEhDepositoCheque = !TipoMovimentacao.DEPOSITO_CHEQUE.equals(saque.getTipoMovimentacao());
		Boolean naoEhDepositoDinheiro = !TipoMovimentacao.DEPOSITO_DINHEIRO.equals(saque.getTipoMovimentacao());

		Boolean naoEhTransferencia = !TipoMovimentacao.TRANSFERENCIA.equals(saque.getTipoMovimentacao());
		if (naoEhDepositoCheque && naoEhDepositoDinheiro && naoEhTransferencia) {
			throw new TipoMovimentacaoObrigatorioException("Tipo movimentação inválida");
		}
	}

	
}

package br.com.ibm.challenge.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.ibm.challenge.controller.dto.CedulaRetornoDto;
import br.com.ibm.challenge.controller.dto.SaqueRetornoDto;
import br.com.ibm.challenge.domain.Movimentacao;
import br.com.ibm.challenge.domain.Transferencia;
import br.com.ibm.challenge.domain.type.TipoMovimentacao;
import br.com.ibm.challenge.repository.MovimentacaoRepository;
import br.com.ibm.challenge.service.exception.CaixaObrigatorioException;
import br.com.ibm.challenge.service.exception.ContaObrigatorioException;
import br.com.ibm.challenge.service.exception.TipoMovimentacaoObrigatorioException;
import br.com.ibm.challenge.service.exception.ValorSaqueSolicitadoInvalidoException;

@Service
public class SaqueService {

	
	private MovimentacaoRepository saqueRepository;
	private ContaService contaService;
	private CaixaService caixaService;
	

	@Autowired
	public void init(MovimentacaoRepository saqueRepository, ContaService contaService, CaixaService caixaService) {
		this.saqueRepository = saqueRepository;
		this.contaService = contaService;
		this.caixaService = caixaService;
	}

	@Transactional
	public SaqueRetornoDto sacar(Movimentacao saque) {
		validaMovimentacaoDeSaque(saque);
		
		SaqueRetornoDto saqueRetorno = montaSaqueRetornoComCedulas(saque);
		
		var caixa = caixaService.findByData(saque.getCaixa().getData());
		var conta = contaService.findById(saque.getConta().getId());
		
		caixaService.validaCaixaObrigatorio(saque, caixa);
		contaService.ajustaSaldoContaNoSaque(saque, conta);
		
		//transformando em negativo
		saque.setConta(conta);
		saque.setCaixa(caixa);
		saque.setVlrMovimento(saque.getVlrMovimento().abs().multiply(new BigDecimal("-1")));
		
		saqueRepository.save(saque);

		return saqueRetorno;
	}

	public void sacar(Transferencia transferencia) {
		var caixa = caixaService.findCaixaAberto();

		var movimentacao = Movimentacao.builder()
				.caixa(caixa)
				.conta(transferencia.getContaOrigem())
				.vlrMovimento(transferencia.getVlrTransferencia())
				.tipoMovimentacao(TipoMovimentacao.TRANSFERENCIA)
				.transferencia(transferencia)
				.build();
		
		this.sacar(movimentacao);
	}

	private SaqueRetornoDto montaSaqueRetornoComCedulas(Movimentacao saque) {
		SaqueRetornoDto saqueRetorno = montaSaqueRetorno(saque.getVlrMovimento());
		Boolean naoParseourCedulas = (saqueRetorno == null);
		if (naoParseourCedulas) {
			throw new ValorSaqueSolicitadoInvalidoException();
		}
		return saqueRetorno;
	}

	private void validaMovimentacaoDeSaque(Movimentacao saque) {
		Boolean naoTemValorMovinentacao = saque.getVlrMovimento() == null;
		if (naoTemValorMovinentacao) {
			throw new ValorSaqueSolicitadoInvalidoException();
		}
		
		Boolean naoTemCaixa = (saque.getCaixa() == null);
		if (naoTemCaixa) {
			throw new CaixaObrigatorioException();
		}
		
		Boolean naoTemConta = (saque.getConta() == null);
		if (naoTemConta) {
			throw new ContaObrigatorioException();
		}
		
		Boolean naoEhSaque = !TipoMovimentacao.SAQUE.equals(saque.getTipoMovimentacao());
		Boolean naoEhTransferencia = !TipoMovimentacao.TRANSFERENCIA.equals(saque.getTipoMovimentacao());
		if (naoEhSaque && naoEhTransferencia) {
			throw new TipoMovimentacaoObrigatorioException("Tipo movimentação inválida");
		}
		
	}
	
	private SaqueRetornoDto montaSaqueRetorno(BigDecimal valor) {
		var cedulasRetorno = listaCedulasRetornoDto(valor);
		
		Boolean temValor = valor.compareTo(BigDecimal.ZERO) > 0;
		Boolean naoRecuperouCedulas = CollectionUtils.isEmpty(cedulasRetorno); 
		
		if (temValor && naoRecuperouCedulas) {
			return null;
		}

		var saqueRetorno = SaqueRetornoDto.builder()
				.cedulasRetorno(cedulasRetorno)
				.build();
		
		return saqueRetorno;
	} 
	
	public List<CedulaRetornoDto> listaCedulasRetornoDto(BigDecimal vlrMovimentacao) {
		int[] ceds = {100, 50, 20, 10, 5, 2};
		
		List<CedulaRetornoDto> cedulas = new ArrayList<CedulaRetornoDto>();
		for (int i = 0 ; i < ceds.length; i++) {
			var cedula = ceds[i];
			var quantidadeCedulas = (int)vlrMovimentacao.doubleValue() / cedula;
			
			if (quantidadeCedulas > 0) {
				cedulas.add(CedulaRetornoDto.builder().cedula(cedula).quantidade(quantidadeCedulas).build());
			}
			
			vlrMovimentacao = vlrMovimentacao.subtract(new BigDecimal(cedula * quantidadeCedulas));
		}
		
		if (vlrMovimentacao.compareTo(new BigDecimal("0")) >0) {
			return null;
		}
		
		return cedulas;
	}


}

package br.com.ibm.challenge.service.exception;

public class CaixaNaoExisteException extends BaseException {
	public CaixaNaoExisteException(String msg) {
		super(msg);
	}

}

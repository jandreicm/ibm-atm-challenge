package br.com.ibm.challenge.service.exception;

public class ContaObrigatorioException extends BaseException {
	public ContaObrigatorioException() {
		super("Conta obrigatória");
	}
}

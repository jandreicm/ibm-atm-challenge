package br.com.ibm.challenge.service.exception;

public class CaixaAbertoDiferenteDoInformadoException extends BaseException {
	public CaixaAbertoDiferenteDoInformadoException() {
		super("Caixa informado é diferente do caixa que esta aberto");
	}

}

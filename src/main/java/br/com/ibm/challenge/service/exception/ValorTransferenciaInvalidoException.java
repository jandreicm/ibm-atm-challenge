package br.com.ibm.challenge.service.exception;

public class ValorTransferenciaInvalidoException extends BaseException {
	public ValorTransferenciaInvalidoException() {
		super("Valor de transferência inválido");
	}
}

package br.com.ibm.challenge.service.exception;

public class CaixaAbertoExistenteException extends BaseException {
	public CaixaAbertoExistenteException(String mensagem) {
		super(mensagem);
	}

}

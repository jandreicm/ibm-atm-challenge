package br.com.ibm.challenge.service.exception;

public class TipoMovimentacaoObrigatorioException extends BaseException {

	private static final long serialVersionUID = -340733502787398287L;

	public TipoMovimentacaoObrigatorioException() {
		super("Tipo movimentação é obrigatório");
	}

	public TipoMovimentacaoObrigatorioException(String mensagem) {
		super(mensagem);
	}

}

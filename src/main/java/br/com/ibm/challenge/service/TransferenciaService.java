package br.com.ibm.challenge.service;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ibm.challenge.controller.dto.ComprovanteRetornoDto;
import br.com.ibm.challenge.domain.Transferencia;
import br.com.ibm.challenge.domain.type.StatusTransferencia;
import br.com.ibm.challenge.repository.TransferenciaRepository;
import br.com.ibm.challenge.service.exception.ContaObrigatorioException;
import br.com.ibm.challenge.service.exception.ValorTransferenciaInvalidoException;

@Service
public class TransferenciaService {

	private TransferenciaRepository transferenciaRepository;
	
	private SaqueService saqueService;
	private DepositoService depositoService;
	
	private ContaService contaService;

	@Autowired
	public void init(
			TransferenciaRepository transferenciaRepository, 
			SaqueService saqueService,
			ContaService contaService,
			DepositoService depositoService
			) {
		this.transferenciaRepository = transferenciaRepository;
		this.saqueService = saqueService;
		this.contaService = contaService;
		this.depositoService = depositoService;
	}

	@Transactional
	public ComprovanteRetornoDto transferencia(Transferencia transferencia) {
		validaTransferencia(transferencia);
		
		Boolean temDataAgendamento = transferencia.getDtAgendamento() != null;
		Boolean dataAgendamentoParaHoje = temDataAgendamento && transferencia.getDtAgendamento().isEqual(LocalDate.now());
		if (!temDataAgendamento || dataAgendamentoParaHoje) {
			transferencia.setStatusTransferencia(StatusTransferencia.IMEDIATO);
		}
		transferencia = this.transferenciaRepository.save(transferencia);
		
		if (!temDataAgendamento || dataAgendamentoParaHoje) {
			//TODO usar JMS ou algo no tipo publish/subscribe para essa tarefa
			this.disparaTransferencia(transferencia);
		}
		
		return montaMensagemDeposito(transferencia);
	}

	private void validaTransferencia(Transferencia transferencia) {
		Boolean naoTemContaOrigem = transferencia.getContaOrigem() == null;
		Boolean naoTemContaDestino = transferencia.getContaDestino() == null;
		Boolean valorTransferenciaInvalido = (transferencia.getVlrTransferencia() == null || transferencia.getVlrTransferencia().compareTo(BigDecimal.ZERO)<=0);
		if (naoTemContaOrigem) {
			throw new ContaObrigatorioException();
		}
		if (naoTemContaDestino) {
			throw new ContaObrigatorioException();
		}
		if(valorTransferenciaInvalido) {
			throw new ValorTransferenciaInvalidoException();
		}
	}
	
	private void disparaTransferencia(Transferencia transferencia) {
		transferencia.setContaDestino(contaService.findById(transferencia.getContaDestino().getId()));
		transferencia.setContaOrigem(contaService.findById(transferencia.getContaOrigem().getId()));
		
		saqueService.sacar(transferencia);
		depositoService.deposito(transferencia);
	}

	
	private ComprovanteRetornoDto montaMensagemDeposito(Transferencia transferencia) {
		var nf = NumberFormat.getCurrencyInstance();
		nf.setGroupingUsed(true);
		nf.setMaximumFractionDigits(2);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy" );
		
		var mensagemRetorno = new StringBuilder();
		mensagemRetorno.append("Transferência efetuado com sucesso.");
		mensagemRetorno.append(String.format("\nConta Origem: %s", transferencia.getContaOrigem()));
		mensagemRetorno.append(String.format("\nConta Destino: %s", transferencia.getContaDestino()));
		mensagemRetorno.append(String.format("\nValor: %s",nf.format(transferencia.getVlrTransferencia().doubleValue())));
		mensagemRetorno.append(String.format("\nData: %s", formatter.format(LocalDate.now())));
		mensagemRetorno.append(String.format("\nSituacao: %s", transferencia.getStatusTransferencia().toString()));
		
		var depositoRetorno = ComprovanteRetornoDto.builder()
				.mensagemRetorno(mensagemRetorno.toString())
				.build();
		return depositoRetorno;
	}

}

package br.com.ibm.challenge.domain.type;

public enum StatusTransferencia {
	
	IMEDIATO, AGENDADO, CONCLUIDO, FALHA
}

package br.com.ibm.challenge.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.ibm.challenge.domain.type.StatusCaixa;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "caixa")
@EqualsAndHashCode(of = "id")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Caixa{

	@Id
	@GeneratedValue
	@Getter @Setter
	private UUID id;
	
	@Getter @Setter @NotNull
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate data;
	
	@Getter @Setter @Enumerated(EnumType.STRING)
	private StatusCaixa statusCaixa;
	
	@Getter @Setter
	private BigDecimal vlrSaldoInicial;
	
	@Getter @Setter
	private BigDecimal vlrSaldoFinal;
	
}

package br.com.ibm.challenge.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.ibm.challenge.domain.type.StatusTransferencia;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "transferencia")
@EqualsAndHashCode(of = "id")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Transferencia {

	@Id
	@GeneratedValue
	@Getter @Setter
	private UUID id;
	
	@Getter @Setter @JoinColumn(name = "conta_origem_id")
	@NotNull
	@OneToOne(fetch = FetchType.LAZY)
	private Conta contaOrigem;
	
	@Getter @Setter  @JoinColumn(name = "conta_destino_id")
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	private Conta contaDestino;
	
	@Getter @Setter
	private LocalDate dtAgendamento;
	
	
	@Getter @Setter @NotNull
	private BigDecimal vlrTransferencia;
	
	@Getter @Setter @Enumerated(EnumType.STRING)
	private StatusTransferencia statusTransferencia;
	
	@Getter @Setter
	private String txtDescricaoFalha;
	
	@Getter @Setter @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private LocalDateTime dtInclusao;
	
	@PrePersist
	public void evento() {
		this.dtInclusao = LocalDateTime.now();
	}


}

package br.com.ibm.challenge.domain.type;

public enum TipoMovimentacao {
	
	SAQUE, DEPOSITO_DINHEIRO, DEPOSITO_CHEQUE, TRANSFERENCIA;

}

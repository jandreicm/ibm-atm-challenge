package br.com.ibm.challenge.domain;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "conta")
@EqualsAndHashCode(of = "id")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Conta {

	@Id
	@GeneratedValue
	@Getter @Setter
	private UUID id;
	
	@Getter @Setter
	private String txtCliente;
	
	@Getter @Setter
	private BigDecimal vlrSaldo;

	
	@Override
	public String toString() {
		return String.format("{Conta=%s, cliente=%s}", this.getId(), this.getTxtCliente());
	}
}

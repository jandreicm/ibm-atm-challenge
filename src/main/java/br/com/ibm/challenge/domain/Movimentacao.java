package br.com.ibm.challenge.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.ibm.challenge.domain.type.TipoMovimentacao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "movimentacao")
@EqualsAndHashCode(of = "id")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Movimentacao{

	@Id
	@GeneratedValue
	@Getter @Setter
	private UUID id;
	
	@Getter @Setter @NotNull @JoinColumn(name = "conta_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private Conta conta;
	
	@Getter @Setter @NotNull 
	@JoinColumn(name = "caixa_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private Caixa caixa;
	
	@Getter @Setter
	private BigDecimal vlrMovimento;
	
	@Getter @Setter @Enumerated(EnumType.STRING) @NotNull
	private TipoMovimentacao tipoMovimentacao;
	
	@Getter @Setter @JoinColumn(name = "transferencia_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private Transferencia transferencia;

	@Getter @Setter @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private LocalDateTime dtInclusao;
	
	@PrePersist
	public void evento() {
		this.dtInclusao = LocalDateTime.now();
	}
	
	
}

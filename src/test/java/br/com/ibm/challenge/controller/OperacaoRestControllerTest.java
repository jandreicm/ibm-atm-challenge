package br.com.ibm.challenge.controller;


import static org.hamcrest.Matchers.hasSize;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import br.com.ibm.challenge.domain.Caixa;
import br.com.ibm.challenge.domain.Conta;
import br.com.ibm.challenge.domain.Movimentacao;
import br.com.ibm.challenge.domain.Transferencia;
import br.com.ibm.challenge.domain.type.TipoMovimentacao;
import br.com.ibm.challenge.repository.CaixaRepository;
import br.com.ibm.challenge.repository.ContaRepository;
import br.com.ibm.challenge.repository.MovimentacaoRepository;
import br.com.ibm.challenge.repository.TransferenciaRepository;
import br.com.ibm.challenge.service.CaixaService;
import br.com.ibm.challenge.service.ContaService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class OperacaoRestControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ContaService contaService;
	@Autowired
	private CaixaService caixaService;
	
	
	@Autowired
	private ContaRepository contaRepository;
	@Autowired
	private CaixaRepository caixaRepository;
	@Autowired
	private MovimentacaoRepository movimentacaoRepository;
	@Autowired
	private TransferenciaRepository transferenciaRepository;
	
	ObjectMapper mapper;
	
	@Before
	public void beforeCaixaTest() {
		movimentacaoRepository.deleteAll();
		transferenciaRepository.deleteAll();
		caixaRepository.deleteAll();
		contaRepository.deleteAll();
		
		mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
	}
	
	private Conta deveCriarConta() {
		var conta = Conta.builder()
				.txtCliente("CLIENTE " + UUID.randomUUID().toString())
				.build();
		
		conta = contaService.criarConta(conta);
		
		return conta;
	}
	
	private Caixa deveCriarEAbrirCaixa() {
		var caixa = Caixa.builder()
				.data(LocalDate.now())
				.build();
		
		caixa = caixaService.criarCaixaSeNaoExiste(caixa);
		caixa = caixaService.abrirCaixa(caixa);

		return caixa;
	}
	
	private Movimentacao criaObjetoMovimentacaoDeposito(Caixa caixa, Conta conta) {
		var movimentacao= Movimentacao.builder()
				.conta(conta)
				.caixa(caixa)
				.vlrMovimento(new BigDecimal("250"))
				.build();
		
		return movimentacao;
	}
	
	@Test
	public void teste_inclui_deposito_dinheiro() throws Exception {
		var conta = deveCriarConta();
		var caixa = deveCriarEAbrirCaixa();
		var movimentacao = criaObjetoMovimentacaoDeposito(caixa, conta);
		movimentacao.setTipoMovimentacao(TipoMovimentacao.DEPOSITO_DINHEIRO);
		
		this.mockMvc.perform(MockMvcRequestBuilders.post("/operacao/deposito")
				.content(this.mapper.writeValueAsString(movimentacao))
				.contentType(MediaType.APPLICATION_JSON))
		
		.andExpect(MockMvcResultMatchers.status().isOk())
        ;
	}
	
	@Test
	public void teste_inclui_deposito_cheque() throws Exception {
		var conta = deveCriarConta();
		var caixa = deveCriarEAbrirCaixa();
		var movimentacao = criaObjetoMovimentacaoDeposito(caixa, conta);
		movimentacao.setTipoMovimentacao(TipoMovimentacao.DEPOSITO_CHEQUE);
		
		this.mockMvc.perform(MockMvcRequestBuilders.post("/operacao/deposito")
				.content(this.mapper.writeValueAsString(movimentacao))
				.contentType(MediaType.APPLICATION_JSON))
		
		.andExpect(MockMvcResultMatchers.status().isOk())
        ;
	}
	
	@Test
	public void teste_inclui_deposito_tipo_movimentacao_errado() throws Exception {
		var conta = deveCriarConta();
		var caixa = deveCriarEAbrirCaixa();
		var movimentacao = criaObjetoMovimentacaoDeposito(caixa, conta);
		movimentacao.setTipoMovimentacao(TipoMovimentacao.SAQUE);
		
		this.mockMvc.perform(MockMvcRequestBuilders.post("/operacao/deposito")
				.content(this.mapper.writeValueAsString(movimentacao))
				.contentType(MediaType.APPLICATION_JSON))
		
		.andExpect(MockMvcResultMatchers.status().is4xxClientError())
        ;
	}

	
	@Test
	public void teste_inclui_saque_sem_saldo() throws Exception {
		var conta = deveCriarConta();
		var caixa = deveCriarEAbrirCaixa();
		var movimentacao = criaObjetoMovimentacaoDeposito(caixa, conta);
		movimentacao.setTipoMovimentacao(TipoMovimentacao.SAQUE);
		
		this.mockMvc.perform(MockMvcRequestBuilders.post("/operacao/saque")
				.content(this.mapper.writeValueAsString(movimentacao))
				.contentType(MediaType.APPLICATION_JSON))
		
		.andExpect(MockMvcResultMatchers.status().is4xxClientError())
        ;
	}

	@Test
	public void teste_inclui_saque_com_saldo() throws Exception {
		var conta = deveCriarConta();
		var caixa = deveCriarEAbrirCaixa();
		var movimentacao = criaObjetoMovimentacaoDeposito(caixa, conta);
		movimentacao.setTipoMovimentacao(TipoMovimentacao.DEPOSITO_DINHEIRO);
		
		this.mockMvc.perform(MockMvcRequestBuilders.post("/operacao/deposito")
				.content(this.mapper.writeValueAsString(movimentacao))
				.contentType(MediaType.APPLICATION_JSON))
		
		.andExpect(MockMvcResultMatchers.status().isOk())
        ;
		
		
		movimentacao = criaObjetoMovimentacaoDeposito(caixa, conta);
		movimentacao.setTipoMovimentacao(TipoMovimentacao.SAQUE);
		movimentacao.setVlrMovimento(new BigDecimal("10"));
		
		this.mockMvc.perform(MockMvcRequestBuilders.post("/operacao/saque")
				.content(this.mapper.writeValueAsString(movimentacao))
				.contentType(MediaType.APPLICATION_JSON))
		
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.jsonPath("$.cedulasRetorno").isArray())
        .andExpect(MockMvcResultMatchers.jsonPath("$.cedulasRetorno", hasSize(1)))
        ;
	}

	
	@Test
	public void teste_inclui_transferencia() throws Exception {
		var conta1 = deveCriarConta();
		var conta2 = deveCriarConta();
		var caixa = deveCriarEAbrirCaixa();
		var movimentacao = criaObjetoMovimentacaoDeposito(caixa, conta1);
		movimentacao.setTipoMovimentacao(TipoMovimentacao.DEPOSITO_DINHEIRO);
		
		this.mockMvc.perform(MockMvcRequestBuilders.post("/operacao/deposito")
				.content(this.mapper.writeValueAsString(movimentacao))
				.contentType(MediaType.APPLICATION_JSON))
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.status().isOk())
        ;

		
		var transferencia = Transferencia.builder()
				.contaOrigem(conta1)
				.contaDestino(conta2)
				.vlrTransferencia(new BigDecimal("100"))
				.build();
		
		this.mockMvc.perform(MockMvcRequestBuilders.post("/operacao/transfere")
				.content(this.mapper.writeValueAsString(transferencia))
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
        ;
	}
	
	
	
	@Test
	public void teste_inclui_transferencia_sem_saldo() throws Exception {
		var conta1 = deveCriarConta();
		var conta2 = deveCriarConta();
		
		deveCriarEAbrirCaixa();
		
		var transferencia = Transferencia.builder()
				.contaOrigem(conta1)
				.contaDestino(conta2)
				.vlrTransferencia(new BigDecimal("100"))
				.build();
		
		this.mockMvc.perform(MockMvcRequestBuilders.post("/operacao/transfere")
				.content(this.mapper.writeValueAsString(transferencia))
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().is4xxClientError())
        ;
	}
}

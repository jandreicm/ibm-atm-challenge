package br.com.ibm.challenge.controller;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import br.com.ibm.challenge.controller.dto.AbreFechaCaixaDto;
import br.com.ibm.challenge.domain.Caixa;
import br.com.ibm.challenge.domain.type.StatusCaixa;
import br.com.ibm.challenge.repository.CaixaRepository;
import br.com.ibm.challenge.service.CaixaService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CaixaRestControllerTest {

	@Autowired
	private CaixaRepository caixaRepository;

	@Autowired
	private CaixaService caixaService;

	@Autowired
	private MockMvc mockMvc;
	
	ObjectMapper mapper;
	
	@Before
	public void beforeCaixaTest() {
		caixaRepository.deleteAll();
		
		mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
	}

	@Test
	public void abre_caixa_ok() throws Exception {
		var caixa = deveCriarCaixa();
		
		String json = mapper.writeValueAsString(caixa);
		
		mockMvc.perform(post("/caixa/abrir")
        		.content(json)
        		.contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$.data").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$.statusCaixa").isNotEmpty())
        ;
	}

	@Test
	public void criar_caixa_ok() throws Exception {
		var caixa = AbreFechaCaixaDto.builder()
				.data(LocalDate.now())
				.vlrSaldoInicial(new BigDecimal("200"))
				.build();
		
		String json = mapper.writeValueAsString(caixa);
		
		mockMvc.perform(post("/caixa/criar")
        		.content(json)
        		.contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$.data").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$.vlrSaldoInicial").value(new BigDecimal("200")))
        .andExpect(MockMvcResultMatchers.jsonPath("$.statusCaixa").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$.statusCaixa").value(StatusCaixa.FECHADO.toString()))
        ;
	}

	@Test
	public void fechar_caixa_ok() throws Exception {
		//cria o caixa e fecha ele.
		var caixa = deveCriarEAbrirCaixa();
		
		String json = mapper.writeValueAsString(caixa);
		
		json = mapper.writeValueAsString(caixa);
		
		mockMvc.perform(post("/caixa/fechar")
        		.content(json)
        		.contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$.data").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$.statusCaixa").value(StatusCaixa.FECHADO.toString()))
        ;
	}

	@Test
	public void extrato_caixa_nao_existe() throws Exception {
		mockMvc.perform(get("/caixa/extrato")
        		.contentType(MediaType.APPLICATION_JSON)
        		.param("caixa", UUID.randomUUID().toString()))
        .andExpect(MockMvcResultMatchers.status().is4xxClientError())
        ;
	}

	
	@Test
	public void extrato_caixa_sem_movimento() throws Exception {
		Caixa caixa = deveCriarCaixa();
		
		mockMvc.perform(get("/caixa/extrato")
        		.contentType(MediaType.APPLICATION_JSON)
        		.param("caixa", caixa.getId().toString()))
        .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
        ;
	}

	@Test
	public void extrato_caixa_sem_id_caixa_informado() throws Exception {
		mockMvc.perform(get("/caixa/extrato")
        		.contentType(MediaType.APPLICATION_JSON)
        		.param("caixa", ""))
        .andExpect(MockMvcResultMatchers.status().is4xxClientError())
        ;
	}

	private Caixa deveCriarEAbrirCaixa() {
		var caixa = Caixa.builder()
				.data(LocalDate.now())
				.vlrSaldoInicial(new BigDecimal("10"))
				.build();
		
		caixa = caixaService.criarCaixaSeNaoExiste(caixa);
		return caixaService.abrirCaixa(caixa);
	}
	
	private Caixa deveCriarCaixa() {
		var caixa = Caixa.builder()
				.data(LocalDate.now())
				.vlrSaldoInicial(new BigDecimal("10"))
				.build();
		
		return caixaService.criarCaixaSeNaoExiste(caixa);
	}

}

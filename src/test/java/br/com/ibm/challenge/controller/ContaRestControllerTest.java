package br.com.ibm.challenge.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import br.com.ibm.challenge.domain.Conta;
import br.com.ibm.challenge.repository.ContaRepository;
import br.com.ibm.challenge.repository.MovimentacaoRepository;
import br.com.ibm.challenge.repository.TransferenciaRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ContaRestControllerTest {

	@Autowired
	private ContaRepository contaRepository;

	@Autowired
	private MovimentacaoRepository movimentacaoRepository;
	
	@Autowired
	private TransferenciaRepository transferenciaRepository;
	

	@Autowired
	private MockMvc mockMvc;
	
	ObjectMapper mapper;
	@Before
	public void beforeCaixaTest() {
		mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
	}
	
	private void limpa() {
		movimentacaoRepository.deleteAll();
		transferenciaRepository.deleteAll();
		contaRepository.deleteAll();
	}

	@Test
	public void cria_conta_ok() throws Exception {
		limpa();
		
		Conta conta = Conta.builder()
				.txtCliente("CLIENTE 1")
				.vlrSaldo(new BigDecimal("100"))
				.build();
		
		String json = mapper.writeValueAsString(conta);
		
		mockMvc.perform(post("/conta")
        		.content(json)
        		.contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$.txtCliente").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$.vlrSaldo").isNotEmpty())
        ;
	}

	@Test
	public void cria_conta_existente() throws Exception {
		Conta conta = Conta.builder()
				.txtCliente("CLIENTE 1")
				.vlrSaldo(new BigDecimal("100"))
				.build();
		
		String json = mapper.writeValueAsString(conta);
		
		mockMvc.perform(post("/conta")
        		.content(json)
        		.contentType(MediaType.APPLICATION_JSON))
		.andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().is4xxClientError())
        ;
	}

}

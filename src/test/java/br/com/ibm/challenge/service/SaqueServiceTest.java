package br.com.ibm.challenge.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.ibm.challenge.controller.dto.SaqueRetornoDto;
import br.com.ibm.challenge.domain.Caixa;
import br.com.ibm.challenge.domain.Conta;
import br.com.ibm.challenge.domain.Movimentacao;
import br.com.ibm.challenge.domain.type.StatusCaixa;
import br.com.ibm.challenge.domain.type.TipoMovimentacao;
import br.com.ibm.challenge.repository.CaixaRepository;
import br.com.ibm.challenge.repository.ContaRepository;
import br.com.ibm.challenge.repository.MovimentacaoRepository;
import br.com.ibm.challenge.service.exception.CaixaNaoExisteException;
import br.com.ibm.challenge.service.exception.CaixaObrigatorioException;
import br.com.ibm.challenge.service.exception.ContaObrigatorioException;
import br.com.ibm.challenge.service.exception.SaldoContaInsuficienteException;
import br.com.ibm.challenge.service.exception.SaldoContaZeradoException;
import br.com.ibm.challenge.service.exception.ValorSaqueSolicitadoInvalidoException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SaqueServiceTest {

	@Spy
	private SaqueService saqueService;

	@Spy
	private ContaService contaService;

	@Spy
	private CaixaService caixaService;

	@Mock
	private MovimentacaoRepository movimentacaoRepository;

	@Mock
	private ContaRepository contaRepository;

	@Mock
	private CaixaRepository caixaRepository;

	
	@Before
	public void configure() {
		this.caixaService.init(caixaRepository, movimentacaoRepository);
		this.contaService.init(contaRepository, movimentacaoRepository);
		this.saqueService.init(movimentacaoRepository, contaService, this.caixaService);
	}
	
	private Movimentacao criaMovimentacao() {
		Conta contaSaque = Conta.builder()
				.vlrSaldo(new BigDecimal("100"))
				.txtCliente("CONTA 1")
				.id(UUID.randomUUID())
				.build();
		
		Caixa caixaSaque = Caixa.builder()
				.data(LocalDate.of(2019, 5, 1))
				.statusCaixa(StatusCaixa.ABERTO)
				.build();
		
		Movimentacao saque = Movimentacao.builder()
				.conta(contaSaque)
				.caixa(caixaSaque)
				.tipoMovimentacao(TipoMovimentacao.SAQUE)
				.vlrMovimento(new BigDecimal("25"))
				.build();
		
		return saque;
	}

	@Test
	public void test_saque_ok() {
		Movimentacao saque = criaMovimentacao();
		Conta contaSaque = saque.getConta();

		saque.getConta().setVlrSaldo(new BigDecimal("100"));
		saque.setVlrMovimento(new BigDecimal("25"));

		Mockito.when(movimentacaoRepository.save(Mockito.any())).thenReturn(saque);
		Mockito.when(contaRepository.getOne(Mockito.any())).thenReturn(contaSaque);
		Mockito.when(caixaRepository.existsByData(Mockito.any())).thenReturn(true);
		Mockito.when(caixaRepository.findByData(Mockito.any())).thenReturn(saque.getCaixa());
		Mockito.when(caixaRepository.findOneByStatusCaixa(Mockito.any())).thenReturn(saque.getCaixa());
		
		SaqueRetornoDto saqueRetorno = saqueService.sacar(saque);
		
		assertTrue(saqueRetorno.getCedulasRetorno().stream().anyMatch(item->item.getCedula().equals(20)));
		assertTrue(saqueRetorno.getCedulasRetorno().stream().anyMatch(item->item.getCedula().equals(5)));
		assertTrue(true);
	}

	@Test(expected = SaldoContaInsuficienteException.class)
	public void test_saque_saldo_insuficiente_exception() {
		Movimentacao saque = criaMovimentacao();
		Conta contaSaque = saque.getConta();

		saque.getConta().setVlrSaldo(new BigDecimal("100"));
		saque.setVlrMovimento(new BigDecimal("250"));

		Mockito.when(movimentacaoRepository.save(Mockito.any())).thenReturn(saque);
		Mockito.when(contaRepository.getOne(Mockito.any())).thenReturn(contaSaque);
		Mockito.when(caixaRepository.existsByData(Mockito.any())).thenReturn(true);
		Mockito.when(caixaRepository.findByData(Mockito.any())).thenReturn(saque.getCaixa());
		Mockito.when(caixaRepository.findOneByStatusCaixa(Mockito.any())).thenReturn(saque.getCaixa());
		
		saqueService.sacar(saque);
	}
	
	@Test(expected = SaldoContaZeradoException.class)
	public void test_saque_saldo_conta_zerada_exception() {
		Movimentacao saque = criaMovimentacao();
		Conta contaSaque = saque.getConta();

		saque.getConta().setVlrSaldo(null);
		saque.setVlrMovimento(new BigDecimal("250"));

		Mockito.when(movimentacaoRepository.save(Mockito.any())).thenReturn(saque);
		Mockito.when(contaRepository.getOne(Mockito.any())).thenReturn(contaSaque);
		Mockito.when(caixaRepository.existsByData(Mockito.any())).thenReturn(true);
		Mockito.when(caixaRepository.findByData(Mockito.any())).thenReturn(saque.getCaixa());
		Mockito.when(caixaRepository.findOneByStatusCaixa(Mockito.any())).thenReturn(saque.getCaixa());
		
		saqueService.sacar(saque);
	}

	@Test(expected = ContaObrigatorioException.class)
	public void test_saque_conta_obrigatoria_exception() {
		Movimentacao saque = criaMovimentacao();
		Conta contaSaque = saque.getConta();

		saque.getConta().setVlrSaldo(new BigDecimal("250"));
		saque.setVlrMovimento(new BigDecimal("250"));

		Mockito.when(movimentacaoRepository.save(Mockito.any())).thenReturn(saque);
		Mockito.when(contaRepository.getOne(Mockito.any())).thenReturn(contaSaque);
		Mockito.when(caixaRepository.existsByData(Mockito.any())).thenReturn(true);
		
		saque.setConta(null);
		saqueService.sacar(saque);
	}
	
	@Test(expected = ValorSaqueSolicitadoInvalidoException.class)
	public void test_saque_valor_saque_invalido_exception() {
		Movimentacao saque = criaMovimentacao();
		Conta contaSaque = saque.getConta();

		saque.getConta().setVlrSaldo(new BigDecimal("2500"));
		saque.setVlrMovimento(new BigDecimal("258"));

		Mockito.when(movimentacaoRepository.save(Mockito.any())).thenReturn(saque);
		Mockito.when(contaRepository.getOne(Mockito.any())).thenReturn(contaSaque);
		Mockito.when(caixaRepository.existsByData(Mockito.any())).thenReturn(true);
		
		saqueService.sacar(saque);
	}
	
	@Test(expected = CaixaNaoExisteException.class)
	public void test_saque_caixa_nao_existe_exception() {
		Movimentacao saque = criaMovimentacao();
		Conta contaSaque = saque.getConta();

		saque.getConta().setVlrSaldo(new BigDecimal("2500"));
		saque.setVlrMovimento(new BigDecimal("250"));

		Mockito.when(movimentacaoRepository.save(Mockito.any())).thenReturn(saque);
		Mockito.when(contaRepository.getOne(Mockito.any())).thenReturn(contaSaque);
		Mockito.when(caixaRepository.existsByData(Mockito.any())).thenReturn(false);
		
		saqueService.sacar(saque);
	}
	
	@Test(expected = CaixaObrigatorioException.class)
	public void test_saque_caixa_obrigatorio_exception() {
		Movimentacao saque = criaMovimentacao();
		Conta contaSaque = saque.getConta();

		saque.getConta().setVlrSaldo(new BigDecimal("2500"));
		saque.setVlrMovimento(new BigDecimal("250"));

		Mockito.when(movimentacaoRepository.save(Mockito.any())).thenReturn(saque);
		Mockito.when(contaRepository.getOne(Mockito.any())).thenReturn(contaSaque);
		Mockito.when(caixaRepository.existsByData(Mockito.any())).thenReturn(true);
		Mockito.when(caixaRepository.findByData(Mockito.any())).thenReturn(saque.getCaixa());
		
		saque.setCaixa(null);
		
		saqueService.sacar(saque);
	}

	
	@Test
	public void teste_vlr_movimentacao_valido() {
		var valoresValidos = new Double[] { 2.0, 5.0, 10.0, 20.0, 50.0, 100.0};
		for (Double valor : valoresValidos) {
			valida(valor, true);
		}
	}
	
	@Test
	public void teste_vlr_movimentacao_invalidos() {
		valida(1.0, false);
		valida(3.0, false);
		valida(4.0, true);
		valida(8.0, false);
		valida(5.5, false);
	}
	
	private void valida(Double valor, Boolean saidaEsperada) {
		var lista = saqueService.listaCedulasRetornoDto(new BigDecimal(valor));
			
		Boolean resultado = lista != null;
		assertEquals(saidaEsperada, resultado);
	}
}

package br.com.ibm.challenge.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.ibm.challenge.domain.Caixa;
import br.com.ibm.challenge.domain.Conta;
import br.com.ibm.challenge.domain.Transferencia;
import br.com.ibm.challenge.repository.CaixaRepository;
import br.com.ibm.challenge.repository.ContaRepository;
import br.com.ibm.challenge.repository.MovimentacaoRepository;
import br.com.ibm.challenge.repository.TransferenciaRepository;
import br.com.ibm.challenge.service.exception.ContaObrigatorioException;
import br.com.ibm.challenge.service.exception.ValorTransferenciaInvalidoException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class TransferenciaServiceTest {

	@Spy
	private TransferenciaService transferenciaService;

	@Spy
	private SaqueService saqueService;

	@Spy
	private DepositoService depositoService;
	
	@Spy
	private ContaService contaService;

	@Spy
	private CaixaService caixaService;
	
	@Mock
	private MovimentacaoRepository movimentacaoRepository;

	@Mock
	private ContaRepository contaRepository;

	@Mock
	private CaixaRepository caixaRepository;

	@Mock
	private TransferenciaRepository transferenciaRepository;

	
	@Before
	public void configure() {
		this.caixaService.init(caixaRepository, movimentacaoRepository);
		this.contaService.init(contaRepository, movimentacaoRepository);
		this.saqueService.init(movimentacaoRepository, contaService, caixaService);
		this.depositoService.init(movimentacaoRepository, contaService, caixaService);
		this.transferenciaService.init(transferenciaRepository, saqueService, contaService, depositoService);
	}
	
	private Transferencia criaTransferencia() {
		Conta contaOrigem = Conta.builder()
				.vlrSaldo(new BigDecimal("100"))
				.txtCliente("CONTA 1")
				.id(UUID.randomUUID())
				.build();
		
		Conta contaDestino = Conta.builder()
				.vlrSaldo(new BigDecimal("100"))
				.txtCliente("CONTA 2")
				.id(UUID.randomUUID())
				.build();
		
		
		var transferencia = Transferencia.builder()
				.contaOrigem(contaOrigem)
				.contaDestino(contaDestino)
				.vlrTransferencia(new BigDecimal("20"))
				.build();
		
		return transferencia;
	}

	@Test
	public void test_transferencia_ok() {
		var transferencia = criaTransferencia();

		var caixa = Caixa.builder().data(LocalDate.now()).build(); 

		Mockito.when(transferenciaRepository.save(Mockito.any())).thenReturn(transferencia);
		Mockito.when(caixaRepository.existsByData(Mockito.any())).thenReturn(true);
		Mockito.when(caixaRepository.findByData(Mockito.any())).thenReturn(caixa);
		Mockito.when(contaRepository.getOne(transferencia.getContaOrigem().getId())).thenReturn(transferencia.getContaOrigem());
		Mockito.when(contaRepository.getOne(transferencia.getContaDestino().getId())).thenReturn(transferencia.getContaDestino());
		
		Mockito.when(contaRepository.getOne(transferencia.getContaOrigem().getId())).thenReturn(transferencia.getContaOrigem());
		Mockito.when(contaRepository.getOne(transferencia.getContaDestino().getId())).thenReturn(transferencia.getContaDestino());
		Mockito.when(caixaRepository.findOneByStatusCaixa(Mockito.any())).thenReturn(caixa);
		
		var comprovante = transferenciaService.transferencia(transferencia);
		
		assertNotNull(comprovante);
		assertTrue(true);
	}


	@Test(expected = ContaObrigatorioException.class)
	public void test_conta_origem_obrigatoria() {
		var transferencia = criaTransferencia();
		transferencia.setContaOrigem(null);
		
		Mockito.when(transferenciaRepository.save(Mockito.any())).thenReturn(transferencia);
		
		transferenciaService.transferencia(transferencia);
		
		assertTrue(true);
	}

	@Test(expected = ContaObrigatorioException.class)
	public void test_conta_destino_obrigatoria() {
		var transferencia = criaTransferencia();
		transferencia.setContaDestino(null);
		
		Mockito.when(transferenciaRepository.save(Mockito.any())).thenReturn(transferencia);
		
		transferenciaService.transferencia(transferencia);
		
		assertTrue(true);
	}
	
	@Test(expected = ValorTransferenciaInvalidoException.class)
	public void test_valor_transferencia_obrigatoria() {
		var transferencia = criaTransferencia();
		transferencia.setVlrTransferencia(null);
		
		Mockito.when(transferenciaRepository.save(Mockito.any())).thenReturn(transferencia);
		
		transferenciaService.transferencia(transferencia);
		
		assertTrue(true);
	}
}

package br.com.ibm.challenge.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.ibm.challenge.domain.Caixa;
import br.com.ibm.challenge.domain.type.StatusCaixa;
import br.com.ibm.challenge.repository.CaixaRepository;
import br.com.ibm.challenge.repository.MovimentacaoRepository;
import br.com.ibm.challenge.service.exception.CaixaNaoExisteException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CaixaServiceTest {

	@Spy
	private CaixaService caixaService;

	@Mock
	private CaixaRepository caixaRepository;
	
	@Mock
	private MovimentacaoRepository movimentacaoRepository;
	
	@Before
	public void configure() {
		this.caixaService.init(caixaRepository, movimentacaoRepository);
	}
	
	@Test
	public void test_abre_caixa_ok() {
		UUID id = UUID.randomUUID();
		Caixa caixaNovo = Caixa.builder()
				.id(id)
				.build();
		
		Mockito.when(caixaRepository.save(Mockito.any())).thenReturn(caixaNovo);
		Mockito.when(caixaRepository.findByData(Mockito.any())).thenReturn(caixaNovo);
		Mockito.when(caixaRepository.existsByData(Mockito.any(LocalDate.class))).thenReturn(true);
		Mockito.when(caixaRepository.existsByStatusCaixa(Mockito.any(StatusCaixa.class))).thenReturn(false);
		
		Caixa caixa = Caixa.builder()
				.data(LocalDate.of(2019, 03, 01))
				.vlrSaldoInicial(BigDecimal.ZERO)
				.build();
		
		Caixa caixaAberto = caixaService.abrirCaixa(caixa);
		
		assertNotNull(caixaAberto);
		assertEquals(id, caixaAberto.getId());
	}

	
	@Test
	public void test_cria_caixa_ok() {
		UUID id = UUID.randomUUID();
		Caixa caixaNovo = Caixa.builder()
				.id(id)
				.build();
		
		Mockito.when(caixaRepository.save(Mockito.any())).thenReturn(caixaNovo);
		Mockito.when(caixaRepository.findByData(Mockito.any())).thenReturn(caixaNovo);
		Mockito.when(caixaRepository.existsByData(Mockito.any(LocalDate.class))).thenReturn(false);
		Mockito.when(caixaRepository.existsByStatusCaixa(Mockito.any(StatusCaixa.class))).thenReturn(false);
		
		Caixa caixa = Caixa.builder()
				.data(LocalDate.of(2019, 03, 01))
				.vlrSaldoInicial(BigDecimal.ZERO)
				.build();
		
		Caixa caixaAberto = caixaService.criarCaixaSeNaoExiste(caixa);
		
		assertNotNull(caixaAberto);
		assertEquals(id, caixaAberto.getId());
	}


	@Test(expected = CaixaNaoExisteException.class)
	public void test_abre_caixa_com_caixa_aberto_existente() {
		Mockito.when(caixaRepository.existsByData(Mockito.any(LocalDate.class))).thenReturn(false);
		Mockito.when(caixaRepository.existsByStatusCaixa(Mockito.any(StatusCaixa.class))).thenReturn(true);
		
		Caixa caixa = Caixa.builder()
				.data(LocalDate.of(2019, 03, 01))
				.vlrSaldoInicial(BigDecimal.ZERO)
				.build();
		
		caixaService.abrirCaixa(caixa);
		
		assertTrue("Abriu caixa corretamente", true);
	}
	
	@Test
	public void test_fecha_caixa_ok() {
		Caixa caixa = Caixa.builder()
				.data(LocalDate.of(2019, 03, 01))
				.vlrSaldoInicial(BigDecimal.ZERO)
				.build();
		
		Mockito.when(caixaRepository.existsByData(Mockito.any(LocalDate.class))).thenReturn(true);
		Mockito.when(caixaRepository.existsByStatusCaixa(StatusCaixa.ABERTO)).thenReturn(true);
		Mockito.when(caixaRepository.findByData(Mockito.any())).thenReturn(caixa);
		Mockito.when(movimentacaoRepository.somaPorCaixa(Mockito.any())).thenReturn(new BigDecimal("100"));
		
		caixaService.fecharCaixa(caixa);
		
		assertTrue("Fechou caixa corretamente", true);
	}

	
	@Test(expected = CaixaNaoExisteException.class)
	public void test_fecha_caixa_data_naoExistente() {
		Mockito.when(caixaRepository.existsByData(Mockito.any(LocalDate.class))).thenReturn(false);
		Mockito.when(caixaRepository.existsByStatusCaixa(StatusCaixa.ABERTO)).thenReturn(true);
		
		Caixa caixa = Caixa.builder()
				.data(LocalDate.of(2019, 03, 01))
				.vlrSaldoInicial(BigDecimal.ZERO)
				.build();
		
		caixaService.fecharCaixa(caixa);
		
		assertTrue("Fechou caixa corretamente", true);
	}

	@Test(expected = CaixaNaoExisteException.class)
	public void test_fecha_caixa_data_existente_status_nao() {
		Mockito.when(caixaRepository.existsByData(Mockito.any(LocalDate.class))).thenReturn(true);
		Mockito.when(caixaRepository.existsByStatusCaixa(StatusCaixa.ABERTO)).thenReturn(false);
		
		Caixa caixa = Caixa.builder()
				.data(LocalDate.of(2019, 03, 01))
				.vlrSaldoInicial(BigDecimal.ZERO)
				.build();
		
		caixaService.fecharCaixa(caixa);
		
		assertTrue("Fechou caixa corretamente", true);
	}

}

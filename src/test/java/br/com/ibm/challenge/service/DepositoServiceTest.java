package br.com.ibm.challenge.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.ibm.challenge.domain.Caixa;
import br.com.ibm.challenge.domain.Conta;
import br.com.ibm.challenge.domain.Movimentacao;
import br.com.ibm.challenge.domain.type.StatusCaixa;
import br.com.ibm.challenge.domain.type.TipoMovimentacao;
import br.com.ibm.challenge.repository.CaixaRepository;
import br.com.ibm.challenge.repository.ContaRepository;
import br.com.ibm.challenge.repository.MovimentacaoRepository;
import br.com.ibm.challenge.service.exception.CaixaAbertoDiferenteDoInformadoException;
import br.com.ibm.challenge.service.exception.ValorDepositoInvalidoException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DepositoServiceTest {

	@Spy
	private DepositoService depositoService;

	@Spy
	private ContaService contaService;

	@Spy
	private CaixaService caixaService;

	@Mock
	private MovimentacaoRepository movimentacaoRepository;

	@Mock
	private ContaRepository contaRepository;

	@Mock
	private CaixaRepository caixaRepository;

	
	@Before
	public void configure() {
		this.caixaService.init(caixaRepository, movimentacaoRepository);
		this.contaService.init(contaRepository, movimentacaoRepository);
		this.depositoService.init(movimentacaoRepository, contaService, this.caixaService);
	}
	
	private Movimentacao criaMovimentacaoDinheiro() {
		Conta conta = Conta.builder()
				.vlrSaldo(new BigDecimal("100"))
				.txtCliente("CONTA 1")
				.build();
		
		Caixa caixa = Caixa.builder()
				.data(LocalDate.of(2019, 5, 1))
				.statusCaixa(StatusCaixa.ABERTO)
				.build();
		
		Movimentacao saque = Movimentacao.builder()
				.conta(conta)
				.caixa(caixa)
				.tipoMovimentacao(TipoMovimentacao.DEPOSITO_DINHEIRO)
				.vlrMovimento(new BigDecimal("25"))
				.build();
		
		return saque;
	}

	private Movimentacao criaMovimentacaoCheque() {
		Conta conta = Conta.builder()
				.vlrSaldo(new BigDecimal("100"))
				.txtCliente("CONTA 1")
				.build();
		
		Caixa caixa = Caixa.builder()
				.data(LocalDate.of(2019, 5, 1))
				.statusCaixa(StatusCaixa.ABERTO)
				.build();
		
		Movimentacao saque = Movimentacao.builder()
				.conta(conta)
				.caixa(caixa)
				.tipoMovimentacao(TipoMovimentacao.DEPOSITO_DINHEIRO)
				.vlrMovimento(new BigDecimal("25"))
				.build();
		
		return saque;
	}

	@Test
	public void test_deposito_dinheiro_ok() {
		Movimentacao movimentacao = criaMovimentacaoDinheiro();
		Conta conta = movimentacao.getConta();

		movimentacao.getConta().setVlrSaldo(new BigDecimal("100"));
		movimentacao.setVlrMovimento(new BigDecimal("25"));

		Mockito.when(movimentacaoRepository.save(Mockito.any())).thenReturn(movimentacao);
		Mockito.when(contaRepository.getOne(Mockito.any())).thenReturn(conta);
		Mockito.when(caixaRepository.existsByData(Mockito.any())).thenReturn(true);
		Mockito.when(caixaRepository.findByData(Mockito.any())).thenReturn(movimentacao.getCaixa());
		Mockito.when(caixaRepository.findOneByStatusCaixa(Mockito.any())).thenReturn(movimentacao.getCaixa());
		
		var depositoRetorno = depositoService.deposito(movimentacao);
		
		assertNotNull(depositoRetorno.getMensagemRetorno());
		assertTrue(true);
	}

	@Test
	public void test_deposito_cheque_ok() {
		Movimentacao movimentacao = criaMovimentacaoCheque();
		Conta conta = movimentacao.getConta();

		movimentacao.getConta().setVlrSaldo(new BigDecimal("100"));
		movimentacao.setVlrMovimento(new BigDecimal("25"));

		Mockito.when(movimentacaoRepository.save(Mockito.any())).thenReturn(movimentacao);
		Mockito.when(contaRepository.getOne(Mockito.any())).thenReturn(conta);
		Mockito.when(caixaRepository.existsByData(Mockito.any())).thenReturn(true);
		Mockito.when(caixaRepository.findByData(Mockito.any())).thenReturn(movimentacao.getCaixa());
		Mockito.when(caixaRepository.findOneByStatusCaixa(Mockito.any())).thenReturn(movimentacao.getCaixa());
		
		var depositoRetorno = depositoService.deposito(movimentacao);
		
		assertNotNull(depositoRetorno.getMensagemRetorno());
		assertTrue(true);
	}
	
	@Test(expected = CaixaAbertoDiferenteDoInformadoException.class)
	public void test_deposito_caixa_aberto_diferente_do_informado() {
		Movimentacao movimentacao = criaMovimentacaoDinheiro();
		movimentacao.getCaixa().setId(UUID.randomUUID());
		
		Conta conta = movimentacao.getConta();
		var caixa = Caixa.builder().data(LocalDate.now()).id(UUID.randomUUID()).build();

		movimentacao.getConta().setVlrSaldo(new BigDecimal("100"));
		movimentacao.setVlrMovimento(new BigDecimal("25"));

		Mockito.when(movimentacaoRepository.save(Mockito.any())).thenReturn(movimentacao);
		Mockito.when(contaRepository.getOne(Mockito.any())).thenReturn(conta);
		Mockito.when(caixaRepository.existsByData(Mockito.any())).thenReturn(true);
		Mockito.when(caixaRepository.findByData(Mockito.any())).thenReturn(movimentacao.getCaixa());
		Mockito.when(caixaRepository.findOneByStatusCaixa(Mockito.any())).thenReturn(caixa);
		
		depositoService.deposito(movimentacao);
	}


	
	@Test(expected = ValorDepositoInvalidoException.class)
	public void test_deposito_com_valor_invalido() {
		Movimentacao movimentacao = criaMovimentacaoDinheiro();
		Conta conta = movimentacao.getConta();

		movimentacao.getConta().setVlrSaldo(new BigDecimal("100"));
		movimentacao.setVlrMovimento(null);

		Mockito.when(movimentacaoRepository.save(Mockito.any())).thenReturn(movimentacao);
		Mockito.when(contaRepository.getOne(Mockito.any())).thenReturn(conta);
		Mockito.when(caixaRepository.existsByData(Mockito.any())).thenReturn(true);
		
		depositoService.deposito(movimentacao);
	}
	
}

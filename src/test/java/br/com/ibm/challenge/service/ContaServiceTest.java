package br.com.ibm.challenge.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.ibm.challenge.domain.Conta;
import br.com.ibm.challenge.repository.ContaRepository;
import br.com.ibm.challenge.repository.MovimentacaoRepository;
import br.com.ibm.challenge.service.exception.ContaJaExisteException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ContaServiceTest {

	@Spy
	private ContaService contaService;

	@Mock
	private ContaRepository contaRepository;
	
	@Mock
	private MovimentacaoRepository movimentacaoRepository; 
	
	@Before
	public void configure() {
		this.contaService.init(contaRepository, movimentacaoRepository);
	}
	
	@Test
	public void test_cria_conta_ok() {
		UUID id = UUID.randomUUID();
		Conta contaNovo = Conta.builder()
				.id(id)
				.build();
		
		Mockito.when(contaRepository.save(Mockito.any())).thenReturn(contaNovo);
		Mockito.when(contaRepository.existsByTxtCliente(Mockito.any())).thenReturn(false);
		
		Conta conta = Conta.builder()
				.txtCliente("CLIENTE 1")
				.build();
		
		Conta contaNova= contaService.criarConta(conta);
		
		assertNotNull(contaNova);
		assertEquals(id, contaNova.getId());
	}
	
	@Test(expected = ContaJaExisteException.class)
	public void test_conta_ja_existe_exception() {
		Mockito.when(contaRepository.existsByTxtCliente(Mockito.any())).thenReturn(true);
		
		Conta conta = Conta.builder().build();
		
		contaService.criarConta(conta);
	}
	
}

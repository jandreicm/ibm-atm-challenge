# Descrição de Utilização da API
### Passos para utilização da API: 
- Criar um caixa em POST **/caixa/criar**
- Abrir o caixa em POST **/caixa/abrir**
- Criar contas POST **/conta**
- Consultar contas GET **/conta**
- Efetuar depositos POST **/operacao/deposito**
- Efetuar saques POST **/operacao/saque**
- Efetuar transferencia POST **/operacao/transfere**

### Extratos existentes
- Extrato de uma Conta **/conta/extrato**
- Extrato de um Caixa **/caixa/extrato**

### Postman Utilização
- Na pasta **/docs** existe o **api-ibm-atm.postman_collection.json**
- Se desejar pode ser utilizado a api do swagger disponível em **/swagger-ui.html**
- Consultas no banco utilizando o console integrado com Springboot acessar **/h2-console**
- Utilizar usuario e senha disponíveis em application.properties
- Utilizado **Java 11** para o desenvolvimento

### Local dos bancos de dados
- Copiar os arquivos **/docs/*.db** para a pasta raiz do usuário
- **~/caixadb** banco com dados de uso em desenvolvimento
- **~/caixadb-test** banco com dados de uso dos testes
 
## Lógica de Desenvolvimento
- Estudei como funcionava fluxo de caixa e formas utilizadas para resolver esse problema.
- Iniciei criando os serviços e testes para lidar com Caixa, abrir caixa, validar campos obrigatórios.
- Criei o serviço de criar contas e testes para esse serviço.
- Por ultimo comecei com as operações, deposito, saque e transferência juntamente com seus testes.
- Os primeiros testes criados foram unitários mocando as chamadas a banco
- Fiz os webservices das operações e os testes utilizando o banco de dados de teste para que os dados de desenvolvimento não fossem comprometidos.
- Optei por utilizar h2 database para facilitar o envio de base com dados para testes.
- Por último me preocupei em criar um @ControllerAdvice para melhorar os retornos dos erros.

*** 

# IBM ATM Challenge
Você deve criar uma API de Caixa ATM, onde deve ser possível executar operações de saque, depósito, transferência, extrato e produzir um relatório de fechamento, além de operações e controles que forem necessários.

* Para as operações estarem disponíveis o caixa deve estar aberto, ou seja, deve ser previsto operações de abertura e fechamento de caixa.

### Saque
 - Para operações de saque, devem ser informados os dados necessários como entrada da operação, e retornar as cédulas que serão retiradas do caixa, além de dados complementares em caso de necessidade. 
 
### Depósito
 - Para operações de depósito, devem ser informados os dados necessários como entrada da operação, além do tipo de depósito (DINHEIRO, CHEQUE) e retornar os dados necessários para o comprovante.

### Transferência
 - Para operações de transferência, devem ser informados os dados necessários para a operação das contas origem e destino, com cenários de validação de saldo e possibilidade de agendamento.
 
## Como entregar estes desafios
Você deve *forkar* este projeto e fazer o *push* no seu próprio repositório e enviar o link para o email do recrutador, junto com seu LinkedIn atualizado.

A implementação deve ficar na pasta correspondente ao desafio. Fique à vontade para adicionar qualquer tipo de conteúdo que julgue útil ao projeto, alterar/acrescentar um README com instruções de como executá-lo, etc.

## Critérios de Avaliação
- Clean code;
- Scalability/Performance;
- Flexibility/Extensibility;
- SOC (Separation of Concerns);
- Tratamento de erros e exceções;
- Lógica utilizada para a resolução do exercício.
  
**Observação:**
- Deve-se utilizar linguagem Java em versões 8+;
- A aplicação deve ser em SpringBoot conforme template deste repositório;
- Você não deve fazer um *Pull Request* para este projeto!

## Extras
- Utilização do Docker;
- Programação Funcional;
- Testes unitários ou de integração;
- Explique o processo de resolução do desafio;
- Regras de sugestão de cédulas para melhor distribuição e disponibilidade no caixa;
- Banco de dados, contemplando os dados de contas corrente e informações de cada operação realizada.

